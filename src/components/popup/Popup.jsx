import { h, Component, createRef } from 'preact'
import { createPortal } from 'preact/compat'
import Animation from '../animation/Animation'
import Body from './Body'
import media from '../../helpers/media'

export default class Dialog extends Component {
  static positions = {
    topLeft: 'top left',
    topRight: 'top right',
    bottomLeft: 'bottom left',
    bottomRight: 'bottom right',
  }

  static defaultProps = {
    self: Dialog.positions.topLeft, // позиционирование относительно себя
    anchor: Dialog.positions.topLeft, // позиционирование относительно таргета
    gutterX: 0, // отступы горизонтальные
    gutterY: 0, // отступы вертикальные
    style: {},
    isFixed: false,
    isShow: false,
    onClose: () => {
      console.log('Missing props: onClose')
    }
  }

  ref = createRef()

  state = {
    left: 0,
    top: 0,
    width: null,
  }

  getTargetElement = () => {
    if (this.props.target) {
      return this.props.target
    }

    return this.__P instanceof HTMLElement ? this.__P : this.__P.childNodes[0]
  }

  setPosition = dialogContainer => {
    let position = { top: 0, left: 0 }
    let width = null

    if (media.isDesktop()) {
      position = this.getPosition(dialogContainer)

      if (this.props.targetWidth) {
        width = this.getTargetElement().clientWidth + 2
      }
    }

    this.setState({ top: position.top + 'px', left: position.left + 'px', width: width + 'px' })
  }

  getPosition = dialogElement => {
    const rect = this.getTargetElement().getBoundingClientRect()

    const { top, height, width, left } = rect
    const scrollTop = getScrollTop()
    const offsetTop = scrollTop + top
    const { anchor, self } = this.props
    const position = { top: null, left: null }
    const { isFixed, gutterX, gutterY } = this.props

    const [vertA, horA] = anchor.split(' ')
    position.top = isFixed ? top : offsetTop
    position.left = left

    if (vertA === 'bottom') {
      position.top += height
    }

    if (horA === 'right') {
      position.left += width
    }

    const [vertS, horS] = self.split(' ')

    if (horS === 'right') {
      position.left -= dialogElement.clientWidth + gutterX
    } else {
      position.left += gutterX
    }

    if (vertS === 'bottom') {
      position.top -= dialogElement.clientHeight + gutterY
    } else {
      position.top += gutterY
    }

    return position
  }

  render({ onClose, isShow, isFixed, self, anchor, style, gutterX, gutterY, ...props }, state, context) {
    if (isFixed) {
      style.position = 'fixed'
    }
    return createPortal(
      <Animation
        {...props}
        ref={this.ref}
        onClick={onClose}
        isShow={isShow}
        className='s-popup-layout'
        animationName='s-popup-layout'
        style={{ left: state.left, top: state.top, width: state.width, ...style }}
      >
        <Body setPosition={this.setPosition} onClose={onClose}>{this.props.children}</Body>
      </Animation>,
      document.body
    )
  }
}

function getScrollTop() {
  return window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
}
