import { h, Component, createRef } from 'preact'
import { lock, unlock } from 'tua-body-scroll-lock'
import debounce from 'debounce'
import media from '../../helpers/media'

export default class Body extends Component {
  ref = createRef()
  isLockedScroll = false

  componentWillMount() {
    setTimeout(() => document.addEventListener('click', this.onGlobalClick))
  }

  componentDidMount() {
    this.lockScroll()
    window.addEventListener('resize', this.onResize)
    this.props.setPosition(this.ref.current)
  }

  componentWillUnmount() {
    this.unlockScroll()

    window.removeEventListener('resize', this.onResize)
    document.removeEventListener('click', this.onGlobalClick)
    this.onResize.clear()
  }

  onGlobalClick = () => this.props.onClose()

  lockScroll() {
    if (media.isMobile()) {
      this.isLockedScroll = true
      lock(this.ref.current)
    }
  }

  unlockScroll() {
    if (this.isLockedScroll) {
      this.isLockedScroll = false
      unlock(this.ref.current)
    }
  }

  preventClose = e => e.stopPropagation()

  onResize = debounce(() => {
    if (!this.isLockedScroll) {
      this.lockScroll()
    } else {
      this.unlockScroll()
    }
    this.props.onClose()
  }, 100)

  render(props, state, context) {
    return (
      <div
        onClick={this.preventClose}
        className='s-popup'
        ref={this.ref}
      >
        {props.children}
      </div>
    )
  }
}
