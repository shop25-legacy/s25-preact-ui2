import { h, Component } from 'preact'
import classes from 'classnames'
import nanoid from 'nanoid'
import Placeholder from './common/Placeholder'
import Label from './common/Label'

export default class Input extends Component {
  id = null
  isFocused = false

  componentDidMount() {
    this.focusInput()
  }

  componentDidUpdate(previousProps, previousState, snapshot) {
    this.focusInput()
  }

  componentWillMount() {
    this.id = this.props.id || nanoid()
  }

  focusInput() {
    if (this.props.isFocused && !this.isFocused) {
      this.isFocused = true
      this.base.firstChild.focus()
    }
  }

  onBlur = e => {
    this.isFocused = false

    typeof this.props.onBlur === 'function' && this.props.onBlur(e)
  }

  render({ label, value = '', placeholder, onBlur, isFocused, hasError, hasButton, className, id, type = 'text', ...props }, state, context) {
    return (
      <div
        className={classes('s-input', {
          [className]: className,
          's-input_has_placeholder': placeholder,
          's-input_has_error': hasError,
          's-input_has_button': hasButton,
        })}
      >
        <input
          {...props}
          onBlur={this.onBlur}
          value={value}
          type='text'
          id={this.id}
          placeholder={label}
          className='s-input__input'
        />
        {
          placeholder && <Placeholder id={this.id} text={placeholder}/>
        }
        <Label id={this.id} text={label}/>
        {props.children}
      </div>
    )
  }
}
