import { createContext, h } from 'preact'
import classNames from 'classnames'
import OptionListItem from './OptionListItem'

export const OptionListContext = createContext({})

const OptionList = ({ horizontal = false, onChange, className, ...props }) => {
  return (
    <OptionListContext.Provider value={{ onChange }}>
      <div className={classNames('s-list', { 's-list_horizontal': horizontal, [className]: className })}>
        {props.children}
      </div>
    </OptionListContext.Provider>
  )
}

OptionList.Item = OptionListItem

export default OptionList
