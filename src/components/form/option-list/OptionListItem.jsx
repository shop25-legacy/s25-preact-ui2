import { h } from 'preact'
import classNames from 'classnames'
import { useContext } from 'preact/hooks'
import { OptionListContext } from './OptionList'

export default ({ checked = false, value, label, description, className, ...props }) => {
  const { onChange } = useContext(OptionListContext)
  return (
    <button
      onClick={() => onChange(value)}
      className={classNames({
        's-list__item':            true,
        's-list__item_selectable': true,
        's-list__item_active':     checked,
        [className]:               className,
      })}
      type='button'
    >
      <div className='s-list__item-left'>
        <div className='s-list__title' dangerouslySetInnerHTML={{ __html: label }} />
        <div className='s-list__description' dangerouslySetInnerHTML={{ __html: description }} />
      </div>
      <div className='s-list__item-right'>{props.children}</div>
    </button>
  )
}
