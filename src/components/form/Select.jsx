import { h } from 'preact'
import classes from 'classnames'
import nanoid from 'nanoid'
import Label from './common/Label'
import { useMemo } from 'preact/hooks'

export default ({ label, value = '', options, placeholder, hasError, hasButton, className, id, ...props }) => {
  id = id || nanoid()

  const entries = useMemo(
    () => Object.entries(options),
    [options]
  )

  return (
    <div
      className={classes('s-input', {
        [className]: className,
        's-input_has_error': hasError,
        's-input_has_button': hasButton,
      })}
    >
      <select
        {...props}
        value={value}
        id={id}
        className='s-input__input'
      >
        <option value=''>{placeholder}</option>
        {
          entries.map(([value, label]) => (
            <option value={value}>{label}</option>
          ))
        }
      </select>
      <Label id={id} text={label}/>
      {props.children}
    </div>
  )
}
