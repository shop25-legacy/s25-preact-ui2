import { h } from 'preact'
import classNames from 'classnames'
import { useContext } from 'preact/hooks'
import { OptionsContext } from './ButtonOptions'

export default ({ checked, value, label, children }) => {
  const { onChange } = useContext(OptionsContext)

  const onClick = () => onChange(value)

  return (
    <button
      type='button'
      onClick={onClick}
      className={classNames('s-button-option', { 's-button-option_active': checked })}
    >
      {label}
      {children}
    </button>
  )
}
