import { createContext, h } from 'preact'
import ButtonOption from './ButtonOption'
import { useMemo } from 'preact/hooks'

export const OptionsContext = createContext({})

const ButtonOptions = ({ children, onChange, ...props }) => {
  const contextValue = useMemo(() => ({ onChange }), [onChange])

  return (
    <OptionsContext.Provider value={contextValue}>
      <div className="s-button-options-wrap" {...props}>
        <div className='s-button-options'>{children}</div>
      </div>
    </OptionsContext.Provider>
  )
}

ButtonOptions.Item = ButtonOption

export default ButtonOptions
