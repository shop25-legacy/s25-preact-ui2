import { h } from 'preact'

export default ({ id, text }) => (
  <label className='s-input__label' htmlFor={id} dangerouslySetInnerHTML={{ __html: text }}/>
)
