import { h } from 'preact'

export default ({ id, text }) => (
  <label className='s-input__placeholder' htmlFor={id} dangerouslySetInnerHTML={{ __html: text }}/>
)
