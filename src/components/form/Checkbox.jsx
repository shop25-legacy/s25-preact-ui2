import { h } from 'preact'
import classes from 'classnames'
import nanoid from 'nanoid'

export default ({ label, checked, large, className, id, ...props }) => {
  id = id || nanoid()

  return (
    <div
      className={classes('s-checkbox', {
        [className]: !!className,
        ['s-checkbox_size_big']: large,
      })}
    >
      <input
        {...props}
        checked={checked}
        type='checkbox'
        id={id}
        className='s-checkbox__input'
      />
      <label htmlFor={id} className='s-checkbox__label'>{label}</label>
    </div>
  )
}
