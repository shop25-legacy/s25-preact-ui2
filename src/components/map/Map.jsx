import { h } from 'preact'
import { useEffect, useRef, useState } from 'preact/hooks'
import classes from 'classnames'
import MyLocationButton from './MyLocationButton'

export default ({ onSelectPoint, getPointIconContent, service, config = {}, points, currentPoint, currentCoords, disableScroll, isShow, isLoading, isEmpty, children }) => {
  const input = useRef(null)
  const [initialized, setInit] = useState(false)

  // Инициализация карты
  useEffect(() => {
    if (!initialized && isShow) {
      service.getDefaultIconContent = getPointIconContent
      service
        .init({ ...config, domElementId: input.current, onSelectPoint })
        .then(() => setInit(true))
    }
  }, [input, isShow])

  // Загрузка точек после инициализации
  useEffect(() => {
    if (initialized && points.length) {
      service.loadPoints(points, currentPoint, currentPoint ? 17 : null)
    }
  }, [points, initialized])

  useEffect(() => {
    // Удаление маркера текущей точки
    if (points.length && !currentPoint) {
      service.resetCurrentMarker()
    }
  }, [currentPoint, points])

  // Дизейбл зума при скроле
  useEffect(() => {
    if (initialized && disableScroll) {
      service.leafletMap.scrollWheelZoom.disable()
    }
  }, [disableScroll, initialized])

  useEffect(() => {
    if (currentCoords) {
      service.addMyAddressMarker(currentCoords)
      service.showPointsAroundCurrentCenter()
    }
  }, [currentCoords])

  return (
    <div className={classes(
      's-map-layout',
      {
        's-map-layout_empty':   isEmpty,
        's-map-layout_loading': !isEmpty && (!initialized || isLoading)
      }
    )}>
      <div className="s-map" ref={input} />
      <MyLocationButton service={service} />
      {children}
    </div>
  )
}
