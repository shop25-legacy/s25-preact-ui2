const GeolocationService = {
  getCurrentCoords() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(position => {
        const { latitude, longitude } = position.coords

        resolve({ lat: latitude, lon: longitude })
      }, reject)
    })
  }
}

export default GeolocationService
