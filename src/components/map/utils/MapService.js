import L from 'leaflet'
import 'leaflet.markercluster/dist/leaflet.markercluster'
import loadYmapsScript from './boot/ymaps-script-load'
import extendYmapsLeaflet from './boot/ymaps-extend-leaflet'

class MapService {
  constructor() {
    this.isInited = false
    // instance leaflet карты
    this.leafletMap = null
    // маркер для адреса найденного через поиск
    this.myAddressMarker = null
    // маркер для выбранной точки
    this.currentMarker = null
    // layer, содержащий все маркеры (нужен для очистки и добавления новых маркеров)
    this.currentMarkersLayer = null
    // Объект данных
    this.geoData = null

    // Иконки для каждого типа маркеров
    this.iconMyAddressMarker = null

    this.getDefaultIconContent = () => 'Бесплатно'

    this.config = {
      domElementId: 'map', // id дом элемена в который вставлять карту
      zoomDefault: 13,
      zoomMax: 18,
      zoomMyAddress: 14,
      zoomMaxDecrement: 12,
      iconDefaultClass: 's-map__marker s-map__marker_point',
      iconCurrentClass: 's-map__marker s-map__marker_selected',
      iconMyAddressClass: 's-map__marker s-map__marker-my',
      onSelectPoint: null, // Колбэк при выборе точки
    }
  }

  async init(config) {
    this.config = { ...this.config, ...config }
    const { zoomDefault, zoomMax, domElementId, iconMyAddressClass } = this.config

    extendYmapsLeaflet(L) // Расширяем leaflet для работы с яндекс картами
    await loadYmapsScript()

    this.leafletMap = new L.Map(domElementId, {
      zoom: zoomDefault,
      maxZoom: zoomMax,
      zoomAnimation: false,
    })
    this.iconMyAddressMarker = L.divIcon({ className: iconMyAddressClass })

    const yandexLayer = new L.Yandex()
    // Убираем возможность взаимодействия с элементами яндекс карты
    yandexLayer.on('MapObjectInitialized', (e) => e.mapObject.options.set('yandexMapDisablePoiInteractivity', true))

    this.leafletMap.addLayer(yandexLayer)
    this.isInited = true
  }

  getDefaultIcon(point) {
    return L.divIcon({
      className: this.config.iconDefaultClass,
      html: `<span>${this.getDefaultIconContent(point)}</span>`,
    })
  }

  getSelectedIcon(point) {
    return L.divIcon({
      className: this.config.iconCurrentClass,
      html: `<span>${this.getDefaultIconContent(point)}</span>`,
    })
  }

  // Добавление точек на карту
  loadPoints(points, currentPoint = null, zoom) {
    if (!this.isInited) {
      throw new Error('loadPoints error: сначала запустите метод "init"')
    }

    this.removePoints()

    // Скрываем подсвечивание области где находятся маркеры в кластере
    this.currentMarkersLayer = L.markerClusterGroup({
      showCoverageOnHover: false,
    })

    // Добавляем маркеры по кластерам
    this.geoData = L.geoJson({
      type: 'FeatureCollection',
      features: points,
    }, {
      // заменяем маркеры на свои
      pointToLayer: (point, coords) => {
        const isCurrent = currentPoint && currentPoint.id === point.id
        const icon = isCurrent ? this.getSelectedIcon(point) : this.getDefaultIcon(point)
        const marker = L.marker(coords, { icon })

        if (isCurrent) {
          this.currentMarker = marker
        }

        return marker
      },
    })

    this.currentMarkersLayer.addLayer(this.geoData)
    this.currentMarkersLayer.on('click', this.$onClickMarker)
    this.leafletMap.addLayer(this.currentMarkersLayer)
    this.leafletMap.fitBounds(this.currentMarkersLayer.getBounds())

    // Если есть выбранная точка то центрируем карту по ней
    if (currentPoint !== null) {
      this.setMapCenter(this.getPointCoordsAsObject(currentPoint), zoom || this.config.zoomDefault)
      // Иначе центрируем карту в зависимости от города
    } else {
      const firstPointCoords = this.getPointCoordsAsObject(points[0])

      this.setMapCenter(firstPointCoords)
    }

    return this
  }

  // Удаление всех точек
  removePoints() {
    if (this.currentMarkersLayer !== null) {
      this.leafletMap.removeLayer(this.currentMarkersLayer)
      this.currentMarkersLayer = null
    }

    return this
  }

  getPointCoordsAsObject(point) {
    const [lon, lat] = point.geometry.coordinates

    return { lon, lat }
  }

  // Центрирование карты по массиву координат
  setMapCenter({ lon, lat }, zoom = null) {
    if (zoom === null) {
      const currentZoom = this.leafletMap.getZoom()
      zoom = currentZoom < this.config.zoomDefault ? this.config.zoomDefault : currentZoom
    }

    this.leafletMap.setView({ lng: lon, lat }, zoom)

    return this
  }

  setCurrentMarker(marker) {
    this.currentMarker = marker
    this.currentMarker.setIcon(this.getSelectedIcon(this.currentMarker.feature))

    return this
  }

  resetCurrentMarker() {
    if (this.currentMarker !== null) {
      this.currentMarker.setIcon(this.getDefaultIcon(this.currentMarker.feature))
      this.currentMarker = null
    }

    return this
  }

  addMyAddressMarker({ lon, lat }) {
    this.removeMyAddressMarker()
    this.setMapCenter({ lon, lat }, this.config.zoomMyAddress)
    this.myAddressMarker = L.marker([lat, lon]).setIcon(this.iconMyAddressMarker)
    this.leafletMap.addLayer(this.myAddressMarker)

    return this
  }

  // Увеличивает масштаб до тех пор пока на карте не появится хотябы 1 точка
  showPointsAroundCurrentCenter = () => {
    const decrementZoom = () => {
      const currentZoom = this.leafletMap.getZoom()

      this.leafletMap.setZoom(currentZoom - 1)

      if (currentZoom > this.config.zoomMaxDecrement && this.getVisibleMarkers().length === 0) {
        decrementZoom()
      }
    }

    if (this.getVisibleMarkers().length < 2) {
      decrementZoom()
    }
  }

  removeMyAddressMarker() {
    if (this.myAddressMarker) {
      this.leafletMap.removeLayer(this.myAddressMarker)
    }

    return this
  }

  destroy() {
    this.resetCurrentMarker()
    this.removePoints()

    if (this.leafletMap) {
      this.leafletMap.remove()
      this.leafletMap = null
    }
  }

  // Массив маркеров находящихся в области видимости
  getVisibleMarkers() {
    return this.currentMarkersLayer
      .getLayers()
      .filter(marker => this.leafletMap
        .getBounds()
        .contains(marker.getLatLng()),
      )
  }

  $onClickMarker = event => {
    this.resetCurrentMarker()
    this.setCurrentMarker(event.layer)

    const coords = this.getPointCoordsAsObject(event.sourceTarget.feature)

    this.setMapCenter(coords)

    if (typeof this.config.onSelectPoint === 'function') {
      const point = event.sourceTarget.feature

      this.config.onSelectPoint(point)
    }
  }
}

export default MapService
