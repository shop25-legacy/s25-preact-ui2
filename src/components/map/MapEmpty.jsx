import { Component, h } from 'preact'

const Header = ({children}) => (
  <div className='s-map-empty__header'>{children}</div>
)

const Description = ({children}) => (
  <div className='s-map-empty__description'>{children}</div>
)

const Footer = ({children}) => (
  <div className='s-map-empty__footer'>{children}</div>
)

class MapEmpty extends Component {
  static Header = Header
  static Description = Description
  static Footer = Footer

  render(props, state, context) {
    return (
      <div className='s-map-empty'>{props.children}</div>
    )
  }
}

export default MapEmpty
