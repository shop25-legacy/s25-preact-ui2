import { h } from 'preact'
import { useEffect, useState } from 'preact/hooks'
import GeolocationService from './utils/GeolocationService'
import classes from 'classnames'


export default ({service}) => {
  const [isShowMyAddress, showMyAddress] = useState(false)
  const [isPendingMyPosition, setPendingMyPosition] = useState(false)

  useEffect(async () => {
    if (isShowMyAddress) {
      showMyAddress(false)
      setPendingMyPosition(true)
      const {lat, lon} = await GeolocationService.getCurrentCoords()
      setPendingMyPosition(false)
      service.addMyAddressMarker({lat, lon})
      service.showPointsAroundCurrentCenter()
    }
  }, [isShowMyAddress])

  return (
    <button
      onClick={() => showMyAddress(true)}
      className={classes('s-map__current-position', {'s-map__current-position_pending': isPendingMyPosition})}
    />
  )
}
