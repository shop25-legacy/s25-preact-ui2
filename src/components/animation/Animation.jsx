import { h, Component, createRef } from 'preact'
import classes from 'classnames'
import equal from 'fast-deep-equal'

const statuses = {
  entering: 'entering',
  entered:  'entered',
  leaving:  'leaving',
  leaved:   'leaved',
}

export default class Animation extends Component {
  static defaultProps = {}

  ref = createRef()

  state = {
    status: statuses.leaved,
  }

  componentWillMount() {
    if (this.props.isShow) {
      this.state.status = statuses.entered
    }
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return !(equal(nextProps, this.props) && equal(nextState, this.state))
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    this.onWillUpdate(nextProps.isShow, nextState.status)
  }

  onAnimationEnd = e => {
    if (e.target.classList.contains('s-dialog-layout')) {
      return
    }

    const { status } = this.state

    if (status === statuses.entering) {
      this.setState({ status: statuses.entered })
      return
    }

    if (status === statuses.leaving) {
      this.setState({ status: statuses.leaved })
    }
  }

  onWillUpdate = (isShow, status) => {
    if (isShow && status === statuses.leaved) {
      this.state.status = statuses.entering
      return
    }

    if (!isShow && status === statuses.entered) {
      this.state.status = statuses.leaving
    }
  }

  render({ className, isShow, animationName, ...props }, state, context) {
    const { status } = state

    if (status === statuses.leaved) {
      return null
    }

    return (
      <div
        onAnimationEnd={this.onAnimationEnd}
        ref={this.ref}
        {...props}
        className={classes({
          [className]:                   className,
          [`${animationName}-entering`]: status === statuses.entering,
          [`${animationName}-entered`]:  status === statuses.entered,
          [`${animationName}-leaving`]:  status === statuses.leaving,
        })}
      >
        {props.children}
      </div>
    )
  }
}
