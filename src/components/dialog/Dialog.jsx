import { h, Component } from 'preact'
import { createPortal } from 'preact/compat'
import classes from 'classnames'
import Body from './Body'
import Footer from './Footer'
import Header from './Header'
import Animation from '../animation/Animation'

export default class Dialog extends Component {
  static Header = Header
  static Body = Body
  static Footer = Footer

  preventClose = e => e.stopPropagation()

  render({ onClose, onBack, isShow, hasBack, className, ...props }, state, context) {
    return createPortal(
      <Animation
        {...props}
        onClick={onClose}
        isShow={isShow}
        className='s-dialog-layout'
        animationName='s-dialog-layout'
      >
        <div
          onClick={this.preventClose}
          className={classes({ [className]: className }, 's-dialog')}
        >
          {this.props.children}
          {
            hasBack &&
            <button className='s-dialog__back' onClick={onBack} />
          }
          <button onClick={onClose} className='s-dialog__close' />
        </div>
      </Animation>,
      document.body
    )
  }
}
