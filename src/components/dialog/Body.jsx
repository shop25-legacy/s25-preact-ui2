import { Component, createRef, h } from 'preact'
import classes from 'classnames'
import { lock, unlock } from 'tua-body-scroll-lock'

export default class Body extends Component {
  ref = createRef()

  componentDidMount() {
    lock(this.ref.current)
  }

  componentWillUnmount() {
    unlock(this.ref.current)
  }

  render({ children, className, ...props }, state, context) {
    return (
      <div
        ref={this.ref}
        className={classes('s-dialog__body', { [className]: className })}
        {...props}
      >
        {children}
      </div>
    )
  }
}
