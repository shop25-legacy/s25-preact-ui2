import { h } from 'preact'
import classes from 'classnames'

export default ({ isSticky = true, children, className, ...props }) => (
  <div
    {...props}
    className={classes('s-dialog__footer', {
      's-dialog__footer_sticky': isSticky,
      [className]: className,
    })}
  >
    {children}
  </div>
)
