import { h } from 'preact'
import classes from 'classnames'

export default ({ isSticky, onBack, onClose, children, nowrap, className, ...props }) => (
  <div
    {...props}
    className={classes('s-dialog__header', {
      's-dialog__header_sticky': isSticky,
      [className]: className
    })}
  >
    <div className={classes('s-dialog__title', { 'text-ellipsis': nowrap })}>
      {children}
    </div>
  </div>
)
