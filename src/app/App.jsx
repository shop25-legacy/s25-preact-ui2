import { h, Component } from 'preact'
import { Router } from 'preact-router'
import Home from './routes/home'
import Buttons from './routes/buttons'
import Sidebar, { routes } from './layout/Sidebar'
import Cards from './routes/cards'
import Typography from './routes/typography'
import Grid from './routes/grid'
import Dialog from './routes/dialog'
import Form from './routes/form'
import Popup from './routes/popup'
import Map from './routes/map'

export default class App extends Component {
  render() {
    return (
      <div className='d-layout'>
        <Sidebar/>
        <div className='d-content'>
          <Router>
            <Home path='/'/>
            <Typography path={routes.typography.href}/>
            <Grid path={routes.grid.href}/>
            <Buttons path={routes.buttons.href}/>
            <Cards path={routes.cards.href}/>
            <Dialog path={routes.dialog.href}/>
            <Form path={routes.form.href}/>
            <Popup path={routes.popup.href}/>
            <Map path={routes.map.href}/>
          </Router>
        </div>
      </div>
    )
  }
}
