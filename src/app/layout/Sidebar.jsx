import { h } from 'preact'
import { Link } from 'preact-router/match'

export const routes = {
  typography: { name: 'Типография', href: '/typography' },
  grid: { name: 'Гриды и отступы', href: '/grid' },
  cards: { name: 'Карточки', href: '/cards' },
  buttons: { name: 'Кнопки', href: '/buttons' },
  dialog: { name: 'Диалоги', href: '/dialog' },
  popup: { name: 'Popup', href: '/popup' },
  form: { name: 'Форма', href: '/form' },
  menu: { name: 'Меню', href: '/menu' },
  map: { name: 'Карта', href: '/map' },
}

export default () => (
  <div className='d-sidebar'>
    <div className='d-sidebar__list'>
      {Object.values(routes).map(component => (
        <Link
          href={component.href}
          activeClassName='d-sidebar__item_active'
          className='d-sidebar__item'
        >
          {component.name}
        </Link>
      ))}
    </div>
  </div>
)
