import { h } from 'preact'
import { useState } from 'preact/hooks'
import Dialog from '../../../components/dialog'
import AutofocusInput from './AutofocusInput'

export default () => {
  const [isShowDialog1, toggleDialog1] = useState(false)
  const [isShowDialog2, toggleDialog2] = useState(false)

  return (
    <div className='d-page'>
      <div className="s-gutter-x-sm">
        <button className='s-button' onClick={() => toggleDialog1(!isShowDialog1)}>
          Заголовок с переносом текста
        </button>
        <button className='s-button' onClick={() => toggleDialog2(!isShowDialog2)}>
          Заголовок в одну строчку
        </button>
      </div>
      <Dialog isShow={isShowDialog1} onClose={() => toggleDialog1(false)}>
        <Dialog.Header>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </Dialog.Header>
        <Dialog.Body>
          <div>
            <AutofocusInput isFocus={true}/>
          </div>
        </Dialog.Body>
        <Dialog.Footer>Привет</Dialog.Footer>
      </Dialog>
      <Dialog isShow={isShowDialog2} onClose={() => toggleDialog2(false)}>
        <Dialog.Header nowrap>
          Заголовок без переноса текста. Еще какой-то текст
        </Dialog.Header>
        <Dialog.Body>
          <div>
            <AutofocusInput isFocus={true}/>
          </div>
        </Dialog.Body>
        <Dialog.Footer>Привет</Dialog.Footer>
      </Dialog>
    </div>
  )
}
