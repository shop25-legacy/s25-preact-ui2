import { h, Component } from 'preact'

export default class Cards extends Component {
  render() {
    return (
      <div className="d-page">
        <h2>Карточки</h2>
        <div className="s-gutter-md">
          <div className="s-card">
            <div className="s-card__header">
              <div className="s-card__title">Обычная карточка</div>
            </div>
            <div className="s-card__body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores aut consequuntur culpa fugit
              id
              illum ipsum maxime nam nemo nisi non nulla quasi quod similique temporibus tenetur, unde voluptate!
            </div>
          </div>

          <div className="s-card s-card_dense">
            <div className="s-card__header">
              <div className="s-card__title">Компактная карточка</div>
            </div>
            <div className="s-card__body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores aut consequuntur culpa fugit
              id
              illum ipsum maxime nam nemo nisi non nulla quasi quod similique temporibus tenetur, unde voluptate!
            </div>
          </div>

          <div className="s-card">
            <div className="s-card__header s-card__header_dark">
              <div className="s-card__title">Карточка с темным заголовком</div>
            </div>
            <div className="s-card__body">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores aut consequuntur culpa fugit
              id
              illum ipsum maxime nam nemo nisi non nulla quasi quod similique temporibus tenetur, unde voluptate!
            </div>
          </div>
          <div>
            <div className="row s-gutter-x-md">
              <div className="col">
                <div className="s-card s-card_dense s-card_bordered">
                  <div className="s-card__header">
                    <div className="s-card__title">Карточка с бордером</div>
                  </div>
                  <div className="s-card__body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores aut consequuntur culpa
                    fugit
                    id
                    illum ipsum maxime nam nemo nisi non nulla quasi quod similique temporibus tenetur, unde voluptate!
                  </div>
                </div>
              </div>
              <div className="col">
                <div className="s-card s-card_dense s-card_bordered s-card_primary">
                  <div className="s-card__header">
                    <div className="s-card__title"><p>Карточка primary</p></div>
                  </div>
                  <div className="s-card__body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores aut consequuntur culpa
                    fugit
                    id
                    illum ipsum maxime nam nemo nisi non nulla quasi quod similique temporibus tenetur, unde voluptate!
                  </div>
                </div>
              </div>
              <div className="col">
                <div className="s-card s-card_dense">
                  <button className="s-card__close"/>
                  <div className="s-card__header">
                    <div className="s-card__title"><p>Карточка с кнопкой закрыть</p></div>
                  </div>
                  <div className="s-card__body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores aut consequuntur culpa
                    fugit
                    id
                    illum ipsum maxime nam nemo nisi non nulla quasi quod similique temporibus tenetur, unde voluptate!
                  </div>
                </div>
              </div>
              <div className="col">
                <div className="s-card s-card_dense">
                  <button className="s-card__close"/>
                  <div className="s-card__header">
                    <div className="s-card__title text-ellipsis">Карточка с кнопкой закрыть</div>
                  </div>
                  <div className="s-card__body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores aut consequuntur culpa
                    fugit
                    id
                    illum ipsum maxime nam nemo nisi non nulla quasi quod similique temporibus tenetur, unde voluptate!
                  </div>
                </div>
              </div>
              <div className="col">
                <div className="s-card s-card_dense">
                  <button className="s-card__close"/>
                  <div className="s-card__header">
                    <div className="s-card__label">Лейбл в s-card__header</div>
                    <div className="s-card__title">Карточка</div>
                  </div>
                  <div className="s-card__body">
                    <div className="s-card__label">Лейбл в s-card__body</div>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores aut consequuntur culpa.
                    <div className="s-card__label">Лейбл в s-card__body</div>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, quisquam?
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }
}
