import { h } from 'preact'
import Input from '../../../components/form/Input'
import { useState } from 'preact/hooks'
import Select from '../../../components/form/Select'
import Checkbox from '../../../components/form/Checkbox'
import OptionList from '../../../components/form/option-list/OptionList'

export default () => {
  const [inputValue, setInputValue] = useState('')
  const [selectValue, setSelectValue] = useState('')
  const [selectedOption, selectOption] = useState(1)

  const options = [{ name: 'qwe', id: 1 }, { name: 'asdzc', id: 2 }, { name: 'qwe af zx', id: 3 }]

  const onInput = e => {
    setInputValue(e.target.value)
  }

  const onChange = e => {
    setSelectValue(e.target.value)
  }

  return (
    <div className='d-page'>
      <div className="row">
        <div className="col s-gutter-sm">
          <Input onInput={onInput} label='Обычный инпут' value={inputValue}/>
          <Input onInput={onInput} label='Disabled инпут' value={inputValue} className='disabled'/>
          <Input
            onInput={onInput} label='Обычный инпут c фокусом (установка фокуса через props)'
            value={inputValue}
            isFocused
          />
          <Input onInput={onInput} label='Инпут с ошибкой' value={inputValue} hasError/>
          <Input onInput={onInput} label='Инпут с кнопкой' value={inputValue} hasButton>
            <button className='s-button'>Поиск</button>
          </Input>
          <Input
            onInput={onInput} label='Инпут с пользовательким значением при потере фокуса'
            value={inputValue}
            placeholder='123123'
          />
          <p>{inputValue}</p>

          <Select
            onChange={onChange}
            value={selectValue}
            label='Обычный селект'
            options={options}
            placeholder='placeholder'
          />
          <Select
            onChange={onChange}
            value={selectValue}
            label='Селект с ошибкой'
            options={options}
            placeholder='placeholder'
            hasError
          />
          <Select
            onChange={onChange}
            value={selectValue}
            label='Селект с кнопкой'
            options={options}
            placeholder='placeholder'
            hasButton
          >
            <button className='s-button'>OK</button>
          </Select>
          <p>{selectValue}</p>

          {/*<Checkbox/>*/}
          {/*<Checkbox label='Обычный чекбокс'/>*/}
          {/*<Checkbox label='Выбранный чекбокс' checked/>*/}
          {/*<Checkbox label='Выбранный чекбокс' large/>*/}


          <label className='s-radio'>
            йцуйцу
            <input type='radio' name='s-radio' className='s-radio__input'/>
            <span className='s-radio__icon'/>
          </label>

          <label className='s-radio'>
            asd
            <input type='radio' name='s-radio' className='s-radio__input'/>
            <span className='s-radio__icon'/>
          </label>

          <label className='s-radio s-radio_size_large'>
            asd
            <input type='radio' name='s-radio' className='s-radio__input'/>
            <span className='s-radio__icon'/>
          </label>
        </div>

        <div className="col s-gutter-sm">
          <OptionList>
            {
              options.map(option => (
                <OptionList.Item
                  onChange={() => selectOption(option.id)}
                  checked={selectedOption === option.id}
                  label={option.name}
                  description='Описание'
                  value={option.id}
                  key={option.id}
                />
              ))
            }
          </OptionList>
        </div>
      </div>
    </div>
  )
}
