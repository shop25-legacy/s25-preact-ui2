import { h, Component } from 'preact'

export default class Buttons extends Component {
  render() {
    return (
      <div className="d-page">
        <h1>Типография</h1>
        <p>Отличие <code>h*</code> от <code>.text-h*</code> в том что <code>h1</code> добавляет отступ(padding) снизу
        </p>
        <div className="row s-gutter-lg">
          <div>
            <table>
              <tr>
                <td>h1, .text-h1</td>
                <td><h1>Заголовок h1</h1></td>
              </tr>
              <tr>
                <td>h2, .text-h2</td>
                <td><h2>Заголовок h2</h2></td>
              </tr>
              <tr>
                <td>h3, .text-h3</td>
                <td><h3>Заголовок h3</h3></td>
              </tr>
              <tr>
                <td>h4, .text-h4</td>
                <td><h4>Заголовок h4</h4></td>
              </tr>
              <tr>
                <td>h5, .text-h5</td>
                <td><h5>Заголовок h5</h5></td>
              </tr>
              <tr>
                <td>h6, .text-h6</td>
                <td><h6>Заголовок h6</h6></td>
              </tr>
            </table>
          </div>
          <div>
            <table>
              <tbody>
              <tr>
                <td>.text-small</td>
                <td><p className='text-small'>Текст</p></td>
              </tr>
              <tr>
                <td>.text-center</td>
                <td><p className='text-center'>Текст</p></td>
              </tr>
              <tr>
                <td>.text-right</td>
                <td><p className='text-right'>Текст</p></td>
              </tr>
              <tr>
                <td>.text-bold</td>
                <td><p className='text-bold'>Текст</p></td>
              </tr>
              <tr>
                <td>.text-normal</td>
                <td><p className='text-normal'>Текст</p></td>
              </tr>
              <tr>
                <td>.text-no-wrap</td>
                <td><p className='text-no-wrap'>Текст</p></td>
              </tr>
              <tr>
                <td>.text-ellipsis</td>
                <td><p className='text-ellipsis' style={{maxWidth: '60px'}}>Lorem ipsum dolor.</p></td>
              </tr>
              <tr>
                <td>.text-uppercase</td>
                <td><p className='text-uppercase'>Текст</p></td>
              </tr>
              <tr>
                <td>.text-lowercase</td>
                <td><p className='text-lowercase'>Текст</p></td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}
