import { h, Component } from 'preact'
import Map, { Service } from '../../../components/Map'
import MapEmpty from '../../../components/map/MapEmpty'

const service1 = new Service()

const points = [{
  'type': 'Feature',
  'id': 'boxberry500',
  'properties': {
    'id': 500,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690039, Владивосток г, Русская ул, д.2а, строение 3',
    'phones': ['8-800-222-80-00'],
    'description': 'Проезд: автобус №№ - 4, 23, 23л, 33а, 77э, 81, 97, троллейбус №№ - 5,7, маршрут. такси №№ - 50, 43. \nОстановка: "Автовокзал (рынок)". \nОт остановки пройти 50 м в направлении Автовокзала. С торца здания 1 дверь.',
    'schedule': 'пн-вс: 08.00-20.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.90566, 43.1647813] }
}, {
  'type': 'Feature',
  'id': 'boxberry1842',
  'properties': {
    'id': 1842,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690014, Владивосток г, Гоголя ул, д.41, оф. 1324',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка "Студенческая ". \nОт остановки пройти 40 метров до входа в ВГУЭС , театрально-концертный комплекс "Андеграунд", пройти до конца холла мимо кафе, повернуть направо и прямо по коридору -  офис № 1324.\nПарковка 30 минут бесплатно.\nВход в здание строго по паспорту!',
    'schedule': 'пн-пт: 10.00-19.00, сб: 10.00-15.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.905284, 43.125499] }
}, {
  'type': 'Feature',
  'id': 'boxberry33180',
  'properties': {
    'id': 33180,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690005, Владивосток г, Луговая ул, д.21А, пом. 19',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка -  Луговая. \nПримерное расстояние от остановки до Отделения  -  50 м.  \nТРЦ Луговая. \nЭтаж  -  1',
    'schedule': 'вт-пт: 10.00-19.00, сб: 10.00-15.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.936671, 43.111469] }
}, {
  'type': 'Feature',
  'id': 'boxberry1516',
  'properties': {
    'id': 1516,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690000, Владивосток г, Океанский пр-кт, д.69, оф. 12',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка: "Дальпресс". \nОт остановки общественного транспорта «Дальпресс» обойти ТЦ "Галерея" с левой стороны. Вход со стороны ул. Советская. Прямо по коридору, Пункт выдачи расположен по левую руку, бутик № 12.',
    'schedule': 'вт-сб: 10.00-18.45',
    'cod': true,
    'card': false,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.892168, 43.1286042] }
}, {
  'type': 'Feature',
  'id': 'boxberry107',
  'properties': {
    'id': 107,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690074, Владивосток г, Выселковая ул, д.49, строение 8',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка: "Выселковая".\nПримерное расстояние от остановки до Отделения  -  100 метров.\nНежилое 2-этажное здание.\nВход на территорию через синие ворота между автомобильным супермаркетом "Гиперавто" и магазином "Сантехника".',
    'schedule': 'пн-пт: 10.00-19.00, сб: 10.00-15.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.952167, 43.150162] }
}, {
  'type': 'Feature',
  'id': 'boxberry17510',
  'properties': {
    'id': 17510,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690091, Владивосток г, Пологая ул, д.69А',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка  -  "Краевой клинический центр". "Телецентр"    \nПримерное расстояние от остановки до Отделения  -  200 м.\nКрупные вывески  -  "Белый ветер".\nОриентиры, известные места  -  телецентр, администрация г. Владивостока.     \nОфисное здание, 2 этаж.',
    'schedule': 'пн-пт: 10.00-19.00, сб: 10.00-16.00',
    'cod': false,
    'card': false,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.890857, 43.120471] }
}, {
  'type': 'Feature',
  'id': 'boxberry20201',
  'properties': {
    'id': 20201,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690001, Владивосток г, Светланская ул, д.155',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка  -  Авангард.\nПримерное расстояние от остановки до Отделения  -  50 м.\nОриентиры, известные места  -  Стадион Авангард, ДК Ленина, Салон оптики Хамелеон.\nКрупные вывески  -  Ортомед, Хамелеон салон оптики.\nРасположение входа в отделение  -  с торца здания.\nЭтаж  -  1.',
    'schedule': 'пн-вс: 10.00-19.00',
    'cod': false,
    'card': false,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.919603, 43.112679] }
}, {
  'type': 'Feature',
  'id': 'boxberry21258',
  'properties': {
    'id': 21258,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690003, Владивосток г, Адмирала Захарова ул, д.5',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка  -  Верхнепортовая (1-я Морская), бухта Федорова.\nПримерное расстояние от остановки до Отделения  -  100 м.       \nОриентиры, известные места  -  Билетур авиа, жд билеты, туризм.\n5-тиэтажный дом.\nКрупные вывески  -  Билетур.\nРасположение входа в отделение  -  с торца здания.\nЭтаж  -  1.',
    'schedule': 'пн-пт: 10.00-19.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.877463, 43.109724] }
}, {
  'type': 'Feature',
  'id': 'boxberry31620',
  'properties': {
    'id': 31620,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690005, Владивосток г, Ивановская ул, д.19',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка  -  Новоивановская или пл. Луговая.\nПримерное расстояние от остановки до Отделения  -  200 м.  \n9-тиэтажный дом.\nРасположение входа в отделение  -  вход  в отделение Билетур.\nЭтаж  -  1',
    'schedule': 'пн-сб: 08.00-19.00, вс: 09.00-18.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.933383, 43.112285] }
}, {
  'type': 'Feature',
  'id': 'boxberry17508',
  'properties': {
    'id': 17508,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690001, Владивосток г, Пушкинская ул, д.40, оф. 703',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка  -  Дальзавод.\nПримерное расстояние от остановки до Отделения  -  300 м.   \n12-тиэтажный Бизнес - центр.\nРасположение входа в отделение  -  первая дверь направо от лифта.\nЭтаж  -  7',
    'schedule': 'пн-пт: 09.00-18.00',
    'cod': true,
    'card': false,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.910763, 43.114924] }
}, {
  'type': 'Feature',
  'id': 'boxberry5175',
  'properties': {
    'id': 5175,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690049, Владивосток г, Русская ул, д.59\/2',
    'phones': ['8-800-222-80-00'],
    'description': 'Проезд: автобус №№ - 40, 60, 82, 98ц, троллейбус №№ - 5,11, маршрут.такси № - 21.\nОстановка: Спортивный комплекс "Восход"\nОт остановки пройти к ТЦ Заря, отдел "Заказник", 1 этаж, налево',
    'schedule': 'вт-сб: 10.00-19.00',
    'cod': true,
    'card': false,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.925136, 43.169443] }
}, {
  'type': 'Feature',
  'id': 'boxberry501',
  'properties': {
    'id': 501,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690087, Владивосток г, Луговая ул, д.63',
    'phones': ['8-800-222-80-00'],
    'description': 'Проезд: автобус №№ - 2, 3, 3\/10, 4, 10, 16к, 34, 54, 54а, 64, 74, 82, 98д, 98ц, 99, 103, 104, 108, трамвай № - 6, маршрут. такси №№ - 96. \nОстановка: "Поликлиника (Минный городок)".\nОт остановки подняться по лестнице к жилому дому Луговая, 59, повернуть направо и вдоль этого дома дойти до следующего дома Луговая, 63, от арки соединяющей дома Луговая 59 и Луговая 63 второе торговое помещение "Заказник".',
    'schedule': 'пн-сб: 10.00-19.00',
    'cod': true,
    'card': false,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.938036, 43.1224783] }
}, {
  'type': 'Feature',
  'id': 'boxberry17681',
  'properties': {
    'id': 17681,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690087, Владивосток г, Луговая ул, д.85А',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка: "Баляева" (в сторону Луговой).\nПримерное расстояние от остановки до Отделения  -  100  метров.\nОтделение расположено в 6-тиэтажном жилом доме на 1 этаже.\nКрупные вывески  -  "SOLO оптика".\nРасположение входа в отделение  -  отдельный вход с улицы.\nПункт выдачи находится в оптике "СОЛО" .',
    'schedule': 'пн-вс: 10.00-19.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.938369, 43.126999] }
}, {
  'type': 'Feature',
  'id': 'boxberry20681',
  'properties': {
    'id': 20681,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690069, Владивосток г, 100-летия Владивостока пр-кт, д.90',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка -  Вторая Речка.\nПримерное расстояние от остановки до Отделения  -  50 м.     \nОриентиры, известные места  -  Билетур , жд, авиа билеты, туризм.\n5-тиэтажный дом.\nКрупные вывески  -  БИЛЕТУР.\nРасположение входа в отделение  -  с дороги.\nЭтаж  -  1.',
    'schedule': 'пн-пт: 09.00-18.00, сб: 08.00-18.30, вс: 09.00-17.30',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.916243, 43.169489] }
}, {
  'type': 'Feature',
  'id': 'boxberry6470',
  'properties': {
    'id': 6470,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690002, Владивосток г, Красного Знамени пр-кт, д.59, оф. 315',
    'phones': ['8-800-222-80-00'],
    'description': 'Проезд: автобус №№ - 17, 17л, 17т, 23. 23л, 24. 28, 40, 41, 45, 105, 106, 107, 111, 112, маршрут.такси №№ - 14т, 78, 102т, 110т, 114т.\nОстановка: "Гоголя".\nОт остановки пройти  в центральный вход здания ПромстройНИИпроекта, далее на второй этаж, офис 315.',
    'schedule': 'пн-пт: 09.00-19.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.907152, 43.127275] }
}, {
  'type': 'Feature',
  'id': 'boxberry17693',
  'properties': {
    'id': 17693,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690021, Владивосток г, Черемуховая ул, д.22',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка: "Диагностический центр.\nПримерное расстояние от остановки до Отделения  -  50  м.\nОриентиры, известные места  -  ТРЦ "Черемушки", "Ростелеком", Дагностический мед. центр".\nОтделение расположено в 6-тиэтажном доме на 1 этаже.\nКрупные вывески  -  "SOLO Оптика", "Аптека25".\nВход в отделение  через "Аптеку25" (высокое крыльцо со стороны дороги).',
    'schedule': 'пн-пт: 10.00-19.00, сб-вс: 10.00-18.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.904978, 43.099805] }
}, {
  'type': 'Feature',
  'id': 'boxberry62553',
  'properties': {
    'id': 62553,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690089, Владивосток г, Ульяновская ул, д.7',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка -  Стройматериалы.\nПримерное расстояние от остановки до Отделения  -  50 м.\n5-этажное ТЦ" Восток City".\nЭтаж  -  1.\nотдел "Ремонт мобильных телефонов"',
    'schedule': 'пн-пт: 10.00-19.00, сб-вс: 11.00-17.00',
    'cod': true,
    'card': false,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.925918, 43.157581] }
}, {
  'type': 'Feature',
  'id': 'boxberry62766',
  'properties': {
    'id': 62766,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690091, Владивосток г, Алеутская ул, д.41',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка  -  Семеновская.\nПримерное расстояние от остановки до Отделения  -  70 метров.     \n4-этажное здание.\nРасположение входа в отделение  -  со двора , в арку.\nЭтаж  -  1, can help Покупка, продажа, ремонт.',
    'schedule': 'пн-вс: 10.00-19.30',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.883526, 43.119951] }
}, {
  'type': 'Feature',
  'id': 'boxberry63745',
  'properties': {
    'id': 63745,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690011, Владивосток г, Борисенко ул, д.35а, корпус 1',
    'phones': ['8 902 -524-51-55', '  8-800-222-80-00'],
    'description': 'Остановка -  "Борисенко".\nПримерное расстояние от остановки до Отделения  -  100 м.\n1-этажное административное здание.\nЭтаж  -  1.\nРасположение входа в отделение - 2 дверь по лицевой стороне здания.\nОтдел КОФЕ.  RICH COFFEE',
    'schedule': 'пн-вс: 09.00-20.00',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.956146, 43.098758] }
}, {
  'type': 'Feature',
  'id': 'boxberry63863',
  'properties': {
    'id': 63863,
    'delivery_id': 95,
    'courier': 'boxberry',
    'address': '690048, Владивосток г, 100-летия Владивостока пр-кт, д.35',
    'phones': ['8-800-222-80-00'],
    'description': 'Остановка  -  проспект 100-летия Владивостока.\nПримерное расстояние от остановки до Отделения  -  100 м.  \n5-тиэтажный дом.\nОтдельный вход со стороны дороги с торца дома.\nЭтаж  -  1.\nБИЛЕТУР Всероссийская сеть.',
    'schedule': 'пн-пт: 09.00-18.00, сб: 08.00-18.30, вс: 09.00-17.30',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'Boxberry'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.906362, 43.150267] }
}, {
  'type': 'Feature',
  'id': 'iml10934',
  'properties': {
    'id': 10934,
    'delivery_id': 90,
    'courier': 'iml',
    'address': 'Владивосток г., ул. Русская, 2а к.3',
    'phones': ['8-495-988-49-06'],
    'description': 'Проезд: автобус №№ - 4,23,23л,33а,77э,81,97, троллейбус №№ - 5,7, маршрут. такси №№ - 50,43. Остановка: " Автовокзал (рынок)". От остановки пройти 50 м в направлении Автовокзала. С торца здания 1 дверь. ',
    'schedule': 'Пн-Вс 08:00-20:00 (без п-ва)',
    'cod': false,
    'card': false,
    'accounting_system': '',
    'courier_name': 'IML'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.906078, 43.164558] }
}, {
  'type': 'Feature',
  'id': 'iml6110',
  'properties': {
    'id': 6110,
    'delivery_id': 90,
    'courier': 'iml',
    'address': 'Владивосток г., ул. Красного Знамени, 59, офис 315',
    'phones': ['8-495-988-49-06'],
    'description': 'Отделение распологается в здании Промсторойниипроекта на третьем этаже, офис 315. ',
    'schedule': 'Пн-Пт 09:00-19:00 (без п-ва), Сб Вс Вых',
    'cod': false,
    'card': false,
    'accounting_system': '',
    'courier_name': 'IML'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.907132, 43.127282] }
}, {
  'type': 'Feature',
  'id': 'iml6117',
  'properties': {
    'id': 6117,
    'delivery_id': 90,
    'courier': 'iml',
    'address': 'Владивосток г., пр-т Океанский, д.69, бутик 12',
    'phones': ['8-495-988-49-05'],
    'description': 'От остановки общественного транспорта "Дальпресс" обойти Торговый Центр Галерея с левой стороны. Вход со стороны ул. Советская. Прямо по коридору, Пункт выдачи расположен по левую руку, бутик № 12. ',
    'schedule': 'Вт-Сб 10:00-19:00 (без п-ва), Пн Вс Вых',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': 'IML'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.892042, 43.128293] }
}, {
  'type': 'Feature',
  'id': 'iml6116',
  'properties': {
    'id': 6116,
    'delivery_id': 90,
    'courier': 'iml',
    'address': 'Владивосток г., ул. Луговая, д.63',
    'phones': ['8-495-988-49-05'],
    'description': 'С ул. Баляева на пл. Луговую, по главной дороге поворот направо во дворы, 2-я пятиэтажка справа. Вход со стороны главной дороги "Заказник". ',
    'schedule': 'Пн-Пт 10:00-19:00 (без п-ва), Сб Вс Вых',
    'cod': false,
    'card': false,
    'accounting_system': '',
    'courier_name': 'IML'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.937726, 43.121785] }
}, {
  'type': 'Feature',
  'id': 'iml6118',
  'properties': {
    'id': 6118,
    'delivery_id': 90,
    'courier': 'iml',
    'address': 'Владивосток г., ул. Русская, д. 59\/2',
    'phones': ['8-495-988-49-05'],
    'description': 'На общественном транспорте: автобус №: 40, 40э, 60, 82, 98д, 98ц ; троллейбус №: 5, 11 ; маршрутное такси №: 21 до остановки "Больница рыбаков" (Спортивный комплекс Восход). От остановки пройдите 10 метров до ТЦ Заря (напротив ТЦ Бум). Главный вход в ТЦ Заря, двигаться по коридору налево до конца. Бутик №15, расположен по левой стороне. Вывеска "Vladik". ',
    'schedule': 'Вт-Сб 10:00-19:00 (без п-ва), Пн Вс Вых',
    'cod': false,
    'card': false,
    'accounting_system': '',
    'courier_name': 'IML'
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.925136, 43.169443] }
}, {
  'type': 'Feature',
  'id': 'shop4',
  'properties': {
    'id': 4,
    'delivery_id': 101,
    'courier': 'shop',
    'address': 'Владивосток, ул. Зои Космодемьянской, дом 19а',
    'phones': ['+7 (423) 260-15-15'],
    'description': '',
    'schedule': 'Понедельник—пятница: с 9:00 до 18:00\r\nСуббота: с 9:00 до 17:00\r\nВоскресенье: выходной',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': ''
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.897092, 43.085995] }
}, {
  'type': 'Feature',
  'id': 'shop5',
  'properties': {
    'id': 5,
    'delivery_id': 101,
    'courier': 'shop',
    'address': 'Владивосток, ул. Русская, дом 92а (второй этаж)',
    'phones': ['+7 (423) 260-15-15'],
    'description': '',
    'schedule': 'Понедельник—пятница: с 9 до 18\r\nСуббота: с 10 до 17\r\nВоскресенье: выходной',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': ''
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.939337, 43.167318] }
}, {
  'type': 'Feature',
  'id': 'shop6',
  'properties': {
    'id': 6,
    'delivery_id': 101,
    'courier': 'shop',
    'address': 'Владивосток, Мыс Кунгасный, площадка №1 строение 3',
    'phones': ['+7 (423) 260-15-15'],
    'description': '',
    'schedule': 'Понедельник—пятница: с 9:00 до 18:00\r\nСуббота: с 9:00 до 17:00\r\nВоскресенье: выходной',
    'cod': true,
    'card': true,
    'accounting_system': '',
    'courier_name': ''
  },
  'geometry': { 'type': 'Point', 'coordinates': [131.886194, 43.129431] }
}]

export default class Grid extends Component {

  state = {
    isShowMap: true,
    points: {},
    currentPoint: null,
    isPending: true,
    isEmpty: false,
    isShowNotFound: false,
    isNotAllowed: false,
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isPending: false,
        points,
      })
    }, 1000)
  }

  render(props, state, context) {
    return (
      <div className="d-page">
        <h3>Карта</h3>
        <div>
          <button className='s-button' onClick={() => this.setState({ isShowMap: !state.isShowMap })}>Скрыть - ОТкрыть
          </button>
        </div>
        {
          state.isShowMap &&
          <Map
            service={service1}
            points={state.points}
            currentPoint={state.currentPoint}
            onSelectPoint={(currentPoint) => this.setState({ currentPoint })}
            isLoading={state.isPending}
            isEmpty={state.isEmpty | state.isShowNotFound | state.isNotAllowed}
          >
            {
              state.isNotAllowed &&
              <MapEmpty>
                <MapEmpty.Header>ПОиск возможен только в России</MapEmpty.Header>
                <MapEmpty.Description>Какоето описание</MapEmpty.Description>
                <MapEmpty.Footer>
                  <button className='s-button'>Достака от 0р</button>
                </MapEmpty.Footer>
              </MapEmpty>
            }
            {
              state.isShowNotFound &&
              <MapEmpty>
                <MapEmpty.Header>Ничего не найдено</MapEmpty.Header>
                <MapEmpty.Description>Какоето описание</MapEmpty.Description>
                <MapEmpty.Footer>
                  <button className='s-button'>Достака от 0р</button>
                </MapEmpty.Footer>
              </MapEmpty>
            }
          </Map>
        }
        <div>
          <button className='s-button' onClick={() => this.setState({ points: [...points].splice(0, 7) })}>Загрузить
            другие точки
          </button>
        </div>
        <div>
          <button className='s-button' onClick={() => this.setState({ points: points })}>Загрузить все</button>
        </div>
        <div>
          <button className='s-button' onClick={() => this.setState({ isPending: !state.isPending })}>Показать скрыть
            загрузку
          </button>
        </div>
        <div>
          <button className='s-button' onClick={() => this.setState({ isEmpty: !state.isEmpty })}>Показать скрыть пустую
            карту
          </button>
        </div>
        <div>
          <button className='s-button' onClick={() => this.setState({ isShowNotFound: !state.isShowNotFound  })}>Показать что нет
            результата
          </button>
        </div>
        <div>
          <button className='s-button' onClick={() => this.setState({ isNotAllowed: !state.isNotAllowed  })}>
            ПОиск возможен только в России
          </button>
        </div>
      </div>
    )
  }
}
