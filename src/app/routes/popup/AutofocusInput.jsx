import { Component, createRef, h } from 'preact'

export default class AutofocusInput extends Component {
  ref = createRef()

  componentDidMount() {
    if (this.props.isFocus) {
      this.ref.current.focus()
    }
  }

  componentDidUpdate({ isFocus }) {
    if (this.props.isFocus && this.props.isFocus !== isFocus) {
      this.ref.current.focus()
    }
  }

  render(props, state, context) {
    return (
      <input ref={this.ref} type="text" className='s-input__input'/>
    )
  }
}
