import { h } from 'preact'
import { useState } from 'preact/hooks'
import Popup from '../../../components/popup'
import Form from './Form'

export default () => {
  const [isShowPopup1, togglePopup1] = useState(false)
  const [position, setPosition] = useState({
    anchor: Popup.positions.topLeft,
    self: Popup.positions.topLeft,
  })

  return (
    <div className='d-page' style={
      {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        paddingTop: '400px',
      }}>
      <button
        onClick={() => togglePopup1(true)}
        style={{ marginBottom: '200px' }}
        className='s-button'
      >
        Таргет кнопка
        <Popup
          onClose={() => togglePopup1(false)}
          self={position.self}
          anchor={position.anchor}
          gutterY={24}
          gutterX={24}
          isShow={isShowPopup1}
          isFixed
        >
          <h3>Lorem ipsum dolor.</h3>
        </Popup>
      </button>

      <Form onChange={setPosition}/>
    </div>
  )
}
