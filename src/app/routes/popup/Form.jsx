import { Component, h } from 'preact'
import Popup from '../../../components/popup/Popup'

export default class Form extends Component {
  state = {
    anchor: Popup.positions.topLeft,
    self: Popup.positions.topLeft,
  }

  onChange = (type, value) => () => {
    const isVertical = value === 'top' || value === 'bottom'

    let [vert, hor] = this.state[type].split(' ')

    if (isVertical) {
      vert = value
    } else {
      hor = value
    }

    const fullValue = [vert, hor].join(' ')
    this.setState({ [type]: fullValue }, () => this.props.onChange(this.state))
  }

  isSelected(type, value) {
    return this.state[type].indexOf(value) !== -1
  }

  render(props, state, context) {
    return (
      <form>
        <table>
          <thead>
          <tr>
            <th>Anchor origin</th>
            <th>Self origin</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              <div className="row text-no-wrap s-gutter-lg">
                <div className="col">
                  <label>
                    <input onChange={this.onChange('anchor', 'top')} type="radio" name='anchor-v'
                           checked={this.isSelected('anchor', 'top')} value='top'/> top
                  </label>
                </div>
                <div className="col">
                  <label>
                    <input onChange={this.onChange('anchor', 'left')} type="radio" name='anchor-h'
                           checked={this.isSelected('anchor', 'left')} value='left'/> left
                  </label>
                </div>
              </div>
            </td>
            <td>
              <div className="row text-no-wrap s-gutter-lg">
                <div className="col">
                  <label>
                    <input onChange={this.onChange('self', 'top')} type="radio" name='self-v'
                           checked={this.isSelected('self', 'top')} value='top'/> top
                  </label>
                </div>
                <div className="col">
                  <label>
                    <input onChange={this.onChange('self', 'left')} type="radio" name='self-h'
                           checked={this.isSelected('self', 'left')} value='left'/> left
                  </label>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div className="row text-no-wrap s-gutter-lg">
                <div className="col">
                  <label>
                    <input onChange={this.onChange('anchor', 'bottom')} type="radio" name='anchor-v'
                           checked={this.isSelected('anchor', 'bottom')}
                           value='bottom'/> bottom
                  </label>
                </div>
                <div className="col">
                  <label>
                    <input onChange={this.onChange('anchor', 'right')} type="radio" name='anchor-h'
                           checked={this.isSelected('anchor', 'right')}
                           value='right'/> right
                  </label>
                </div>
              </div>
            </td>
            <td>
              <div className="row text-no-wrap s-gutter-lg">
                <div className="col">
                  <label>
                    <input onChange={this.onChange('self', 'bottom')} type="radio" name='self-v'
                           checked={this.isSelected('self', 'bottom')}
                           value='bottom'/> bottom
                  </label>
                </div>
                <div className="col">
                  <label>
                    <input onChange={this.onChange('self', 'right')} type="radio" name='self-h'
                           checked={this.isSelected('self', 'right')} value='right'/> right
                  </label>
                </div>
              </div>
            </td>
          </tr>
          </tbody>
        </table>
      </form>
    )
  }
}
