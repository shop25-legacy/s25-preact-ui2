import { h, Component } from 'preact'

export default class Buttons extends Component {
  render() {
    return (
      <div className="d-page s-gutter-y-md">
        <h2>Кнопки</h2>
        <div className="s-card">
          <div className="s-card__header">
            <div className="s-card__title">Цвета</div>
          </div>
          <div className="s-card__body">
            <table>
              <thead>
              <tr>
                <th/>
                <th>default</th>
                <th>accent</th>
                <th>positive</th>
                <th>secondary</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>Default</td>
                <td>
                  <button className='s-button'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_accent'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_positive'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_secondary'>Кнопка</button>
                </td>
              </tr>
              <tr>
                <td>Rounded</td>
                <td>
                  <button className='s-button s-button_rounded'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_accent s-button_rounded'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_positive s-button_rounded'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_secondary s-button_rounded'>Кнопка</button>
                </td>
              </tr>
              <tr>
                <td>Flat</td>
                <td>
                  <button className='s-button s-button_flat'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_accent s-button_flat'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_positive s-button_flat'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_secondary s-button_flat'>Кнопка</button>
                </td>
              </tr>
              <tr>
                <td>Outline</td>
                <td>
                  <button className='s-button s-button_outline'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_accent s-button_outline'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_positive s-button_outline'>Кнопка</button>
                </td>
                <td>
                  <button className='s-button s-button_secondary s-button_outline'>Кнопка</button>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div className="s-card">
          <div className="s-card__header">
            <div className="s-card__title">Размеры и формы</div>
          </div>
          <div className="s-card__body">
            <div className="s-gutter-md">
              <button className="s-button">По-умолчанию</button>
            </div>
            <button className="s-button s-button_wide" style={{ marginTop: '24px' }}>Растянутая кнопка</button>


            <div className="s-gutter-md" style={{ marginTop: '24px' }}>
              <button className="s-button s-button_size_small">Маленькая</button>
              <button className="s-button">По-умолчанию</button>
              <button className="s-button s-button_size_large">Большая</button>
            </div>
          </div>
        </div>

        <div className="s-card">
          <div className="s-card__header">
            <div className="s-card__title">Disabled</div>
          </div>
          <div className="s-card__body s-gutter-md">
            <button className="s-button s-button_disabled">Кнопка</button>
            <button className="s-button s-button_accent s-button_disabled">Кнопка</button>
            <button className="s-button s-button_positive s-button_disabled">Кнопка</button>
            <button className="s-button s-button_outline s-button_disabled">Кнопка</button>
            <button className="s-button s-button_accent s-button_outline s-button_disabled">Кнопка</button>
            <button className="s-button s-button_positive s-button_outline s-button_disabled">Кнопка</button>
            <button className="s-button s-button_flat s-button_disabled">Кнопка</button>
            <button className="s-button s-button_accent s-button_flat s-button_disabled">Кнопка</button>
            <button className="s-button s-button_positive s-button_flat s-button_disabled">Кнопка</button>
          </div>
        </div>

        <div className="s-card">
          <div className="s-card__header">
            <div className="s-card__title">С иконками</div>
          </div>
          <div className="s-card__body s-gutter-md">
            <div>Все иконки добавляются через background-image. Достаточно добавить дополнительный класс в котором есть
              свойство "background-image: url('hui.svg'), url('hui.svg')" или прописать его в style
            </div>
            <div>Отступы и остальные настройки background НЕ ЗАДАВАТЬ!</div>
            <div className="row s-gutter-x-sm">
              <button className="s-button s-button_has-icon"
                      style={{ backgroundImage: 'url(\'/s-card__close.66613362.svg\')' }}>Кнопка
              </button>
              <button className="s-button s-button_has-icon-right"
                      style={{ backgroundImage: 'url(\'/s-card__close.66613362.svg\')' }}>Кнопка
              </button>
              <button className="s-button s-button_has-icon s-button_has-icon-right"
                      style={{ backgroundImage: 'url(\'/s-card__close.66613362.svg\'), url(\'/s-card__close.66613362.svg\')' }}>Кнопка
              </button>
              <button className="s-button s-button_has-icon"
                      style={{ backgroundImage: 'url(\'/s-card__close.66613362.svg\')' }}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
