import { h, Component } from 'preact'

export default class Grid extends Component {
  render() {
    return (
      <div className="d-page">
        <h1>Гриды и отступы</h1>

        <h4>Строка <code>.row</code></h4>
        <div className="row">
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#333', color: '#fff' }}><code>.col</code></div>
        </div>

        <h4>Строка c отступами<code>.row.s-gutter-xs</code></h4>
        <div className="row s-gutter-xs">
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#333', color: '#fff' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
        </div>
        <h4>Строка c отступами<code>.row.s-gutter-sm</code></h4>
        <div className="row s-gutter-sm">
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#333', color: '#fff' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
        </div>
        <h4>Строка c отступами<code>.row.s-gutter-md</code></h4>
        <div className="row s-gutter-md">
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#333', color: '#fff' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
        </div>
        <h4>Строка c отступами<code>.row.s-gutter-lg</code></h4>
        <div className="row s-gutter-lg">
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#333', color: '#fff' }}><code>.col</code></div>
          <div className="col" style={{ backgroundColor: '#999' }}><code>.col</code></div>
        </div>

        <h4>Отступы только по горизонтали <code>.s-gutter-x-md</code></h4>
        <div className="s-gutter-x-md">
          <button className='s-button'>button.s-button</button>
          <button className='s-button'>button.s-button</button>
          <button className='s-button'>button.s-button</button>
          <button className='s-button'>button.s-button</button>
        </div>

        <h4>Отступы только по вертикали <code>.s-gutter-y-md</code></h4>
        <div className="s-gutter-y-md">
          <div style={{border: '1px solid #eee'}}>.div</div>
          <div style={{border: '1px solid #eee'}}>.div</div>
          <div style={{border: '1px solid #eee'}}>.div</div>
          <div style={{border: '1px solid #eee'}}>.div</div>
        </div>
      </div>
    )
  }
}
