export default {
  isMobile() {
    return window.matchMedia('(max-width: 991px)').matches
  },
  isDesktop() {
    return window.matchMedia('(min-width: 992px)').matches
  },
}
