import debounce from 'debounce'
import createHook from './createHook'

let Handsontable = null

export default class Hot {
  static createHook = createHook

  // Передаем в конструктов, чтобы можно было подключать разные версии
  constructor(handsontable) {
    Handsontable = handsontable
    this.viewData = []
    this.editData = []
    this.editMode = false
    this.findCallback = (value, query) => {
      if (value === null) {
        return false
      }

      return value.toString().toLowerCase().indexOf(query.toLowerCase()) !== -1
    }
    this.minSpareRows = 0
    this.hotInstance = {} // hot instance
    this.config = {}
    this.editConfig = {}
    this.onChangeCallbacks = []
    this.beforeChange = null
    this.filterCallbacks = {}
    this.changes = []
    this.state = {
      queryString:      '',
      filterConditions: {},
      changedRows:      new Set(),
      cleanedRows:      [],
    }
  }

  init(container, config = {}, editConfig = {}) {
    const ht = this
    this.config = config
    this.editConfig = editConfig
    this.hotInstance = createHot(container, { ...this.config })
    this.minSpareRows = config.minSpareRows || 0
    Handsontable.hooks.add('afterChange', [createHook((changes, source) => {
      for (let [rowIndex, prop, oldValue, newValue] of changes) {
        const physicalRowIndex = ht.hotInstance.toPhysicalRow(rowIndex)
        rowIndex = ht.hotInstance.getSourceDataAtRow(physicalRowIndex).hotRowIndex

        // если новая строка, в ней надо проставить hotRowIndex
        // только если таблица не фильтруется, т.к. physicalRowIndex === hotRowIndex только в этом случае
        if (rowIndex === null) {
          rowIndex = physicalRowIndex
          ht.hotInstance.setDataAtRowProp(physicalRowIndex, 'hotRowIndex', physicalRowIndex, 'manual')
        }
        const changeIndex = ht.changes.findIndex(c => c.rowIndex === rowIndex && c.prop === prop)
        const row = ht.viewData[rowIndex]
        oldValue = row ? (row[prop] || null) : null

        if (changeIndex === -1) {
          ht.changes.push({ rowIndex, prop, oldValue, newValue })
        } else {
          ht.changes[changeIndex] = { rowIndex, prop, oldValue, newValue }
        }
      }

      this.changes = this.changes.filter(({ oldValue, newValue }) => oldValue !== newValue)

      for (const cb of this.onChangeCallbacks) {
        if (typeof cb === 'function') {
          cb(changes, source, ht)
        }
      }
    })], this.hotInstance)
    Handsontable.hooks.add('beforeChange', [createHook((changes, source) => {
      if (typeof this.beforeChange === 'function') {
        this.beforeChange(changes, source, ht)
      }
    })], this.hotInstance)
  }

  loadData(data) {
    const dataWithRowIndexes = data.map((rowData, index) => ({ ...rowData, hotRowIndex: index }))
    this.viewData = dataWithRowIndexes
    this.editData = cloneData(dataWithRowIndexes)
    this.reRender()
  }

  // Переход в режим редактирования
  setEditMode() {
    this.editMode = true
    this.hotInstance.updateSettings({ ...this.editConfig, readOnly: false })
    this.reRender()
  }

  // Переход в режим просмотра с отменой всех изменений
  cancelEditMode() {
    this.editMode = false
    this.hotInstance.updateSettings({ ...this.config, readOnly: true })
    this.editData = cloneData(this.viewData)
    this.changes = []
    this.reRender()
  }

  // Переход в режим просмотра с сохранением изменений
  setViewMode() {
    this.editMode = false
    this.hotInstance.updateSettings({ ...this.config, readOnly: true })
    this.viewData = cloneData(this.editData)
    this.changes = []
    this.reRender()
  }

  getChanges() {
    return this.changes
  }

  addAfterChange = cb => {
    this.onChangeCallbacks.push(cb)
  }

  markRowAsChanged(physicalRow) {
    this.state.changedRows.add(physicalRow)
  }

  reRender = () => {
    if (this.state.queryString) {
      this.updateMinSpareRows()
    }

    const currentData = this.getCurrentData()

    this.hotInstance.loadData(currentData)

    if (!this.state.queryString) {
      this.updateMinSpareRows()
    }

    this.highlightSearch()
    this.hotInstance.render()
    this.applySort()
  }

  updateMinSpareRows = () => {
    const queryString = this.state.queryString
    const config = this.editMode ? { ...this.editConfig } : { ...this.config }
    const minSpareRows = queryString !== '' ? 0 : (config.minSpareRows || 0)
    this.hotInstance.updateSettings({ minSpareRows })
  }

  applySort = () => {
    if (this.hotInstance.sortColumn !== undefined && this.hotInstance.sortOrder !== undefined) {
      this.hotInstance.sort(this.hotInstance.sortColumn, this.hotInstance.sortOrder)
    }
  }

  highlightSearch = () => {
    const queryString = this.state.queryString

    if (queryString !== '') {
      const search = this.hotInstance.getPlugin('search') || this.hotInstance.search
      search.query(queryString)
    }
  }

  getCurrentData = () => {
    const queryString = this.state.queryString
    let currentData = this.editMode ? this.editData : this.viewData

    if (Object.keys(this.filterCallbacks).length !== 0) {
      currentData = currentData.filter(
        rowData => Object.values(this.filterCallbacks).every(filterCallback => filterCallback(rowData)),
      )
    }

    if (queryString !== '') {
      currentData = currentData.filter(row => Object.values(row).some(value => this.findCallback(value, queryString)))
    }

    return currentData
  }

  setFilter = (name, callback) => {
    this.filterCallbacks[name] = callback
    this.reRender()
  }

  unsetFilter = (name) => {
    delete this.filterCallbacks[name]
    this.reRender()
  }

  search = debounce(queryString => {
    this.state.queryString = queryString.trim()
    this.reRender()
  }, 250)
}

function createHot(container, config = {}) {
  if (typeof container === 'string') {
    container = document.querySelector('container')
  }

  const baseConfig = {
    ...{
      data:     [],
      stretchH: 'last',
      filters:  true,
      search:   { queryMethod: onlyExactMatch },
      copyPaste: {
        rowsLimit: 100000,
      },
    },
    ...config,
  }

  return new Handsontable(container, baseConfig)
}

function onlyExactMatch(query = null, value = null) {
  if (value === null) {
    return false
  }

  return value.toString().toLowerCase().indexOf(query.toLowerCase()) !== -1
}

function cloneData(data) {
  return data.map(rowData => ({ ...rowData }))
}
