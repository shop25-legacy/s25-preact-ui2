export default class DataLoader {
    sourceData = []

    currentData = []

    loadData(data) {
      this.sourceData = data
      this.currentData = data
    }

    getSourceData() {
      return DataLoader.getWithoutEmptyRows(this.sourceData)
    }

    getClonedData() {
      const cloned = this.sourceData.map(sourceRowData => ({ ...sourceRowData }))
      return DataLoader.getWithoutEmptyRows(cloned)
    }

    setCurrentData(data) {
      this.currentData = data
    }

    getCurrentData() {
      return DataLoader.getWithoutEmptyRows(this.currentData)
    }

    static getWithoutEmptyRows(data) {
      return data.filter(row => Object.keys(row).length > 0)
    }
}
