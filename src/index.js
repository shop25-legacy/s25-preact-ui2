import './app/styles/index.pcss'
import { h, render } from 'preact'
import App from './app/App'

render(h(App), document.querySelector('#app'))

